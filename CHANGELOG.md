# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.0] - 2023-05-25

### Changed

-   upgrade node to v18
-   migrate to npm
