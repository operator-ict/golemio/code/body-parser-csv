"use strict";

import { parse as fastcsvParse } from "fast-csv";
import { Readable } from "stream";
import { ICSVSettings } from "./ICSVSettings";

/**
 * CSV Parser class
 */
export class CSVParser {

    /** fast-csv library parameters */
    private settings: object;

    /** line transformation */
    private subscribe: (json: any) => any;

    constructor(settings: ICSVSettings) {
        this.settings = settings.fastcsvParams || {};
        this.subscribe = settings.subscribe || ((json: any) => json);
    }

    /**
     * parse
     */
    public async parse(data: string | Buffer): Promise<any[]> {
        const readable = new Readable();
        readable.push(data);
        readable.push(null);

        return new Promise((resolve, reject) => {
            const resulsArray = [];
            readable
                .pipe(fastcsvParse(this.settings))
                .on("error", (error) => {
                    reject(error);
                })
                .on("data", (row) => {
                    resulsArray.push(this.subscribe(row));
                })
                .on("end", (rowCount: number) => {
                    return resolve(resulsArray);
                });
        });
    }
}
