"use strict";

import { Options } from "body-parser";
import { ICSVSettings } from "./ICSVSettings";

export interface IExtendedOptions extends Options {
    csvParseOptions?: ICSVSettings;
}
