"use strict";

import { NextFunction, Request, Response } from "express";
import { CSVParser } from "./CSVParser";
import { IExtendedOptions } from "./IExtendedOptions";

const DEFAULT_TYPES = [ "text/csv" ];

/**
 * Extending `body-parser` by property `csv` (CSVParser middleware).
 *
 * Implementation inspired by `body-parser-xml`
 *
 * Usage:
 * const bodyParser = require("body-parser");
 * require("body-parser-csv")(bodyParser);
 * express.use(bodyParser.csv({...}));
 */
export = (bodyParser: any): (req: Request, res: Response, next: NextFunction) => void => {
    if (bodyParser.csv) {
        // We already setup the CSV parser.
        // End early.
        return;
    }

    /// CSV parsing middleware
    const csv = (options: IExtendedOptions): (req: Request, res: Response, next: NextFunction) => void => {
        options = options || {};

        options.type = options.type || DEFAULT_TYPES;
        if (!Array.isArray(options.type)) {
            options.type = [options.type as string];
        }

        const textParser = bodyParser.text(options);

        const csvParser = (req: Request, res: Response, next: NextFunction): void => {
            // First, run the body through the text parser.
            textParser(req, res, (err: Error) => {
                if (err) {
                    return next(err);
                }
                if (typeof req.body !== "string") {
                    return next();
                }

                // Then, parse CSV.
                const parser = new CSVParser(options.csvParseOptions);
                parser.parse(req.body)
                    .then((parsed) => {
                        req.body = parsed || req.body;
                        next();
                    })
                    .catch((error) => {
                        error.status = 400; // TODO: use real error status
                        return next(error);
                    });
            });
        };
        return csvParser;
    };

    // Finally add the `csv` function to the bodyParser.
    Object.defineProperty(bodyParser, "csv", {
        configurable: true,
        enumerable: true,
        get() {
            return csv;
        },
    });
};
