"use strict";

import { ParserOptionsArgs } from "fast-csv";

export interface ICSVSettings {

    /** fast-csv library parameters */
    fastcsvParams?: ParserOptionsArgs;

    /** line transformation */
    subscribe?: (json: any) => any;
}
