# body-parser-csv

This package is inspired by [body-parser-xml](https://www.npmjs.com/package/body-parser-xml) and is using [fast-csv](https://www.npmjs.com/package/fast-csv) for parsing.

Developed by http://operatorict.cz


## Prerequisites

- node.js (https://nodejs.org)
- npm (https://www.npmjs.com/)
- TypeScript (https://www.typescriptlang.org/)

## Installation

Install Node

Install all npm modules using command:
```
npm install
```

## Compilation of typescript code

To compile typescript code into js one-time

```
npm run build
```
or run this, to watch all changes
```
npm run build-watch
```
from the application's root directory.


## Usage

In your project's `package.json` set dependency to body-parser-csv
```
npm install body-parser-csv --save
```

Then import module, e.g.
```
const bodyParser = require("body-parser");
require("body-parser-csv")(bodyParser);
```


You can then use body-parser-csv the same way as you would use the body-parser-xml, with [csv parsing options](https://github.com/C2FO/fast-csv/blob/HEAD/docs/parsing.md) inside of the `csvParseOptions` parameter.

```
this.express.use(
    bodyParser.csv({
        csvParseOptions: {
            fastcsvParams: {
                headers: true,
                trim: true,
            },
            subscribe: ((json: any) => {
                // some line transformation
                return json;
            }),
        },
        limit: "15MB",
    })
);
```

## Documentation

For generating documentation run `npm run generate-docs`. TypeDoc source code documentation is located in `docs/typedoc`.

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact benak@operatorict.cz
