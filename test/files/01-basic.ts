"use strict";

import { expect } from "chai";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as request from "supertest";
import app from "../server";

const bodyParser = require("body-parser");
require("../../src/")(bodyParser);

describe("Basic test", () => {

    let testApp;
    let testData: string;

    before(() => {
        app.post("/test1",
            bodyParser.csv({
                csvParseOptions: {
                    fastcsvParams: {
                        headers: true,
                        trim: true,
                    },
                },
                limit: "15MB",
            }),
            (req: Request, res: Response, next: NextFunction) => {
                res.status(200).send(req.body);
            });
        app.post("/test2",
            bodyParser.csv({
                csvParseOptions: {
                    fastcsvParams: {
                        headers: true,
                        trim: true,
                    },
                    subscribe: ((json: any) => {
                        const a = json.count;
                        delete json.count;
                        json.number = a;
                        return json;
                    }),
                },
                limit: "15MB",
            }),
            (req: Request, res: Response, next: NextFunction) => {
                res.status(200).send(req.body);
            });
        testApp = request(app);
    });

    beforeEach(() => {
        testData = "day,count\r\n"
        + "2018-08-02,5\r\n"
        + "2018-08-20,1\r\n"
        + "2018-08-21,34\r\n"
        + "2018-08-22,25\r\n"
        + "2018-08-23,13\r\n"
        + "2018-08-24,1\r\n"
        + "2018-08-25,10\r\n";
    });

    it("Should return JSON", (done) => {
        testApp
            .post("/test1")
            .send(testData)
            .set("Content-Type", "text/csv")
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.length).to.equal(7);
                const firstItem = res.body[0];
                expect(firstItem).to.have.property("count");
                expect(firstItem.day).to.equal("2018-08-02");
                expect(firstItem.count).to.equal("5");
                done();
            });
    });

    it("Should return JSON transformed by line stransformation", (done) => {
        testApp
            .post("/test2")
            .send(testData)
            .set("Content-Type", "text/csv")
            .expect(200)
            .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.length).to.equal(7);
                const firstItem = res.body[0];
                expect(firstItem).not.to.have.property("count");
                expect(firstItem).to.have.property("number");
                expect(firstItem.day).to.equal("2018-08-02");
                expect(firstItem.number).to.equal("5");
                done();
            });
    });
});
